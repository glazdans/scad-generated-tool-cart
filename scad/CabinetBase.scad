DEFAULT_BOARD_WIDTH = 21;
CabinetWidth = 700;
CabinetLenght = 400;
CabinetHeight = 730;

BackingBoardWidth = 12;

CabinetBoardWidth = 21;

SlideWidth = 12.7;
SlideHeight = 45.7;

MiddleBoardCount = 1;

module cabinet() {
    baseBoard = [CabinetWidth - (CabinetBoardWidth * 2), CabinetLenght - BackingBoardWidth, CabinetBoardWidth];
    sideBoard = [CabinetHeight, CabinetLenght - BackingBoardWidth, CabinetBoardWidth];
    backingBoard = [CabinetWidth, CabinetHeight, BackingBoardWidth];

    //Bottom
    translate([CabinetBoardWidth, 0, 0])board_x_plane(baseBoard, "cabinet_bottom");
    //Middle board
    for(i = [1:MiddleBoardCount]) {
        zOffset = i * (CabinetHeight/ (MiddleBoardCount+1)) - CabinetBoardWidth/2;
        translate([CabinetBoardWidth, 0, zOffset])board_x_plane(baseBoard, str("cabinnet_mid",i));
    }
    //Top board
    translate([CabinetBoardWidth, 0, CabinetHeight - baseBoard.z])board_x_plane(baseBoard, "cabinet_top");
    //Sides
    board_y_plane(sideBoard, "cabinet_left");
    translate([CabinetWidth - CabinetBoardWidth, 0, 0])board_y_plane(sideBoard, "cabinet_right");

    //Back board
    translate([0, CabinetLenght - BackingBoardWidth, 0])board_z_plane(backingBoard, "cabinet_back");
}

// Drawers
SmallDrawerCount = 2;
MediumDrawerCount = 2;
BigDrawerCount = 2;

SmallDrawersRatio = 1;
MediumDrawersRatio = 2;
BigDrawersRatio = 3;

DrawerMargin = 6;
DrawerFaceMargin = 2;

Ratio = 0;
Count = 1;

Small = 0;
Medium = 1;
Big = 2;
// ratio-[Small, Medium, Big], count-[Small, Medium, Big]
DrawerConfiguration = [
    [[1,2,3],[1,2,0]],
    [[1,2,3],[2,2,0]]   
    ];

function drawerCount(config) = config[Count][Small] + config[Count][Medium] + config[Count][Big];

function drawerBaseHeight(config, totalDrawerHeight) = (totalDrawerHeight - (DrawerMargin * 2 * drawerCount(config))) 
    / (
        config[Ratio][Small] * config[Count][Small] 
        + config[Ratio][Medium] * config[Count][Medium] 
        + config[Ratio][Big] * config[Count][Big]
        ) ;

module drawers(){
    assert(len(DrawerConfiguration) == MiddleBoardCount + 1, "There needs to be drawer config for each division");
    echo("Drawers", DrawerConfiguration);
    totalDrawerHeight = (CabinetHeight - (CabinetBoardWidth * (2 + MiddleBoardCount))) 
    / (MiddleBoardCount + 1);
    totalDrawerLength = CabinetLenght - BackingBoardWidth;
    totalDrawerWidth = CabinetWidth - (CabinetBoardWidth * 2);
    echo("Total drawer division dimensions", totalDrawerHeight, totalDrawerLength, totalDrawerWidth);
    // TODO - (SlideWidth * 2);

    baseMarginLeft = CabinetBoardWidth;

    for(i = [0:len(DrawerConfiguration) - 1]) {
        config = DrawerConfiguration[i];
        echo("Config for i", i, config);
        echo("Drawer count", drawerCount(config));
        drawerBaseHeight = drawerBaseHeight(config, totalDrawerHeight);
        echo("DrawerBaseHeight for subdivision: ",i , drawerBaseHeight);
        
        smallDrawerSize = drawerBaseHeight * config[Ratio][Small];
        medimuDrawerSize = drawerBaseHeight * config[Ratio][Medium];
        bigDrawerSize = drawerBaseHeight * config[Ratio][Big];
        echo("Resulting config drawer sizes: ", smallDrawerSize, medimuDrawerSize, bigDrawerSize);

        assert(drawerBaseHeight * SmallDrawersRatio > SlideHeight, "ERROR: Small drawer is smaller than the slide!");

        baseMarginBottom = (i * (CabinetBoardWidth + totalDrawerHeight)) + CabinetBoardWidth;

        if (config[Count][Big] > 0) {
            for(j = [1: config[Count][Big]]) {
                marginBottom = baseMarginBottom
                + ((j - 1) * (bigDrawerSize + (2 * DrawerMargin)))
                + DrawerMargin;
                translate([baseMarginLeft, 0, marginBottom])
                drawer(drawerWidth = totalDrawerWidth,
                drawerLength = totalDrawerLength,
                drawerHeight = bigDrawerSize,
                labelIndex = str("bi",i,"j",j)
                );
            }
        }
        if (config[Count][Medium] > 0) {
            for(j = [1: config[Count][Medium]]) {
                marginBottom = baseMarginBottom 
                + ((config[Count][Big]) * (bigDrawerSize + (2 * DrawerMargin)))
                + ((j - 1) * (medimuDrawerSize + (2 * DrawerMargin)))
                + DrawerMargin;
                translate([baseMarginLeft, 0, marginBottom])
                drawer(drawerWidth = totalDrawerWidth,
                drawerLength = totalDrawerLength,
                drawerHeight = medimuDrawerSize,
                labelIndex = str("mi",i,"j",j)
                );
            }
        }
        if (config[Count][Small] > 0) {
            for(j = [1: config[Count][Small]]) {
                marginBottom = baseMarginBottom 
                + ((config[Count][Big]) * (bigDrawerSize + (2 * DrawerMargin)))
                + ((config[Count][Medium]) * (medimuDrawerSize + (2 * DrawerMargin)))
                + ((j - 1) * (smallDrawerSize + (2 * DrawerMargin)))
                + DrawerMargin;
                translate([baseMarginLeft, 0, marginBottom])
                drawer(drawerWidth = totalDrawerWidth,
                drawerLength = totalDrawerLength,
                drawerHeight = smallDrawerSize,
                labelIndex = str("si",i,"j",j)
                );
            }
        }
    }   
}

DrawerBoardWidth = 12;
module drawer(drawerWidth, drawerLength, drawerHeight, labelIndex){
    frontBoard = [drawerWidth - (DrawerFaceMargin * 2), drawerHeight + (2 * (DrawerMargin - DrawerFaceMargin)), DrawerBoardWidth];

    drawerWidth = drawerWidth - (2 * SlideWidth);
    drawerLength = drawerLength - DrawerMargin;
    baseBoard = [drawerWidth - (DrawerBoardWidth * 2), drawerLength - (2 * DrawerBoardWidth), DrawerBoardWidth];
    sideBoard = [drawerHeight, drawerLength - (2 * DrawerBoardWidth), DrawerBoardWidth];
    backingBoard = [drawerWidth, drawerHeight, DrawerBoardWidth];

    translate([SlideWidth, DrawerBoardWidth, 0]) {
        translate([DrawerBoardWidth, 0, 0])board_x_plane(baseBoard, str("drawer_bottom",labelIndex));
        //Sides
        board_y_plane(sideBoard, str("drawer_left",labelIndex));
        translate([drawerWidth - DrawerBoardWidth, 0, 0])board_y_plane(sideBoard, str("drawer_right",labelIndex));
        //Back board
        translate([0, drawerLength - (2 * DrawerBoardWidth), 0])board_z_plane(backingBoard, str("drawer_back",labelIndex));
    }
    translate([DrawerFaceMargin, 0, -(DrawerMargin - DrawerFaceMargin)])
    board_z_plane(frontBoard, str("drawer_front",labelIndex));
}


// Common boards
module board_z_plane(dimensions, label) {
    translate([0, dimensions.z, 0])rotate([90,0,0])board(dimensions, label);
}
module board_y_plane(dimensions, label) {
    translate([dimensions.z, 0, 0]) rotate([0, -90, 0])board(dimensions, label);
}
module board_x_plane(dimensions, label) {
    board(dimensions, label);
}
module board(dimensions, label) {
    echo("Added board:")
    echo(str("\n    ", label,
    ":\n        width: ", dimensions.x,
    "\n        height: ", dimensions.y,
    "\n        can_rotate: true\n"));
    cube(size=[dimensions.x, dimensions.y, dimensions.z], center=false);
}

cabinet();
#translate([0, 0, 0])drawers();